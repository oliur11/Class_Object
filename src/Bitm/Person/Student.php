<?php
namespace App\Bitm\Person;

class Student{
    
    public $name = "Default Name";
    public $weight = 12;
    
     public function __construct($name=null){
        $this->name = $name;
    }
    
    public function sayHello(){
        echo "Hello! from ".$this->name;
        
    }
    
}
