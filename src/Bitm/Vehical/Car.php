<?php

namespace App\Bitm\Vehical;

class Car{
    public $color = "red";
    public $body  = "wooden";
    public $wheel = 4;
    public $seat  = 4;
    public $brand = "BMW";
    
    public function __construct(){
        echo "<b>I am constructing...</b>";
    }
    
    public function drive(){
        echo "I am running...";
    }
    
}
